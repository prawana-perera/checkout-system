package au.com.dius.shopping;

import java.math.BigDecimal;

public class ProductFixture {

    public static final Product IPAD = new Product("ipd", "Super iPad", BigDecimal.valueOf(549.99));
    public static final Product MAC_BOOK_PRO = new Product("mbp", "MacBook Pro", BigDecimal.valueOf(1399.99));
    public static final Product APPLE_TV = new Product("atv", "Apple TV", BigDecimal.valueOf(109.50));
    public static final Product VGA_ADAPTER = new Product("vga", "VGA adapter", BigDecimal.valueOf(30.00));
}
