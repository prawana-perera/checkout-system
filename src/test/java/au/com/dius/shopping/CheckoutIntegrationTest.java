package au.com.dius.shopping;

import au.com.dius.shopping.rules.BulkDiscountPricingRule;
import au.com.dius.shopping.rules.BundledProductPricingRule;
import au.com.dius.shopping.rules.PricingRule;
import au.com.dius.shopping.rules.XForPriceOfYPricingRule;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static au.com.dius.shopping.ProductFixture.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class CheckoutIntegrationTest {

    private List<PricingRule> pricingRules;
    private Map<String, Product> productCatalog;
    private Checkout checkout;

    @Before
    public void setUp() {

        productCatalog = new HashMap<>();
        productCatalog.put(IPAD.getSku(), IPAD);
        productCatalog.put(MAC_BOOK_PRO.getSku(), MAC_BOOK_PRO);
        productCatalog.put(APPLE_TV.getSku(), APPLE_TV);
        productCatalog.put(VGA_ADAPTER.getSku(), VGA_ADAPTER);

        pricingRules = new ArrayList<>();
        pricingRules.add(new XForPriceOfYPricingRule(APPLE_TV, 3, 2));
        pricingRules.add(new BulkDiscountPricingRule(IPAD, 4, BigDecimal.valueOf(499.99)));
        pricingRules.add(new BundledProductPricingRule(MAC_BOOK_PRO, VGA_ADAPTER));

        checkout = new Checkout(productCatalog, pricingRules);
    }

    @Test
    public void it_should_return_correct_total_for_atv_atv_atv_vga() {

        checkout.scan(APPLE_TV.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(VGA_ADAPTER.getSku());

        BigDecimal total = checkout.total();
        assertThat(total, is(equalTo(BigDecimal.valueOf(249.00))));
    }

    @Test
    public void it_should_return_correct_total_for_atv_ipd_ipd_atv_ipd_ipd_ipd() {

        checkout.scan(APPLE_TV.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(IPAD.getSku());

        BigDecimal total = checkout.total();
        assertThat(total, is(equalTo(BigDecimal.valueOf(2718.95))));
    }

    @Test
    public void it_should_return_correct_total_for_mbp_vga_ipd() {

        checkout.scan(MAC_BOOK_PRO.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(IPAD.getSku());

        BigDecimal total = checkout.total();
        assertThat(total, is(equalTo(BigDecimal.valueOf(1949.98))));
    }

    @Test
    public void it_should_return_correct_total_for_mbp_vga_ipd_vga_vga_mbp() {

        checkout.scan(MAC_BOOK_PRO.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(MAC_BOOK_PRO.getSku());

        BigDecimal total = checkout.total();
        assertThat(total, is(equalTo(BigDecimal.valueOf(3379.97))));
    }

    @Test
    public void it_should_return_correct_total_for_atv_atv_atv_vga_atv_vga_atv_atv() {

        checkout.scan(APPLE_TV.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(APPLE_TV.getSku());

        BigDecimal total = checkout.total();
        assertThat(total, is(equalTo(BigDecimal.valueOf(498.00))));
    }

    @Test
    public void it_should_return_correct_total_for_vga_atv_ipd_atv_atv_ipd_vga_mbp_ipd_ipd_ipd() {

        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(APPLE_TV.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(VGA_ADAPTER.getSku());
        checkout.scan(MAC_BOOK_PRO.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(IPAD.getSku());
        checkout.scan(IPAD.getSku());

        BigDecimal total = checkout.total();
        assertThat(total, is(equalTo(BigDecimal.valueOf(4148.94))));
    }
}
