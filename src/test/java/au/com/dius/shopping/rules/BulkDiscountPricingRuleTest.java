package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static au.com.dius.shopping.ProductFixture.IPAD;
import static au.com.dius.shopping.ProductFixture.MAC_BOOK_PRO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BulkDiscountPricingRuleTest {

    private static final BigDecimal DISCOUNTED_UNIT_PRICE = BigDecimal.valueOf(2.00);

    private BulkDiscountPricingRule rule;
    private List<Item> shoppingCart;

    @Before
    public void setUp() {
        rule = new BulkDiscountPricingRule(IPAD, 3, DISCOUNTED_UNIT_PRICE);
        shoppingCart = new ArrayList<>();
    }

    @Test
    public void apply_should_not_discount_if_shopping_cart_is_empty() {
        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_not_discount_if_number_of_applicable_items_is_less_than_required() {
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));

        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_discount_if_number_of_applicable_items_is_equal_to_required() {

        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));
        shoppingCart.add(new Item(IPAD));

        Optional<PricingRuleResult> pricingRuleResultOptional = rule.apply(shoppingCart);

        assertThat(pricingRuleResultOptional.isPresent(), is(true));

        BigDecimal expectedDiscountedPrice = DISCOUNTED_UNIT_PRICE.multiply(BigDecimal.valueOf(3));
        PricingRuleResult pricingRuleResult = pricingRuleResultOptional.get();
        assertThat(pricingRuleResult.getTotalPriceWithDiscount(), is(equalTo(expectedDiscountedPrice)));
        assertThat(pricingRuleResult.getDiscountedItems(), containsInAnyOrder(shoppingCart.get(0), shoppingCart.get(1), shoppingCart.get(3)));
    }

    @Test
    public void apply_should_discount_if_number_of_applicable_items_is_greater_than_required() {

        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(IPAD));

        Optional<PricingRuleResult> pricingRuleResultOptional = rule.apply(shoppingCart);

        assertThat(pricingRuleResultOptional.isPresent(), is(true));

        BigDecimal expectedDiscountedPrice = DISCOUNTED_UNIT_PRICE.multiply(BigDecimal.valueOf(4));
        PricingRuleResult pricingRuleResult = pricingRuleResultOptional.get();
        assertThat(pricingRuleResult.getTotalPriceWithDiscount(), is(equalTo(expectedDiscountedPrice)));
        assertThat(pricingRuleResult.getDiscountedItems(),
                containsInAnyOrder(shoppingCart.get(0), shoppingCart.get(1), shoppingCart.get(3), shoppingCart.get(4)));
    }
}
