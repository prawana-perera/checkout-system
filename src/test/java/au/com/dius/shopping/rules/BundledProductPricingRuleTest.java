package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static au.com.dius.shopping.ProductFixture.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BundledProductPricingRuleTest {

    private BundledProductPricingRule rule;
    private List<Item> shoppingCart;

    @Before
    public void setUp() {
        rule = new BundledProductPricingRule(MAC_BOOK_PRO, VGA_ADAPTER);
        shoppingCart = new ArrayList<>();
    }

    @Test
    public void apply_should_not_discount_if_shopping_cart_is_empty() {
        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_not_discount_if_required_product_not_in_cart() {
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(VGA_ADAPTER));

        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_not_discount_if_bundled_product_not_in_cart() {
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));

        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_discount_if_both_required_and_bundled_product_found_in_cart() {

        shoppingCart.add(new Item(VGA_ADAPTER));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));

        Optional<PricingRuleResult> pricingRuleResultOptional = rule.apply(shoppingCart);

        assertThat(pricingRuleResultOptional.isPresent(), is(true));

        BigDecimal expectedDiscountedPrice = MAC_BOOK_PRO.getPrice();
        PricingRuleResult pricingRuleResult = pricingRuleResultOptional.get();
        assertThat(pricingRuleResult.getTotalPriceWithDiscount(), is(equalTo(expectedDiscountedPrice)));
        assertThat(pricingRuleResult.getDiscountedItems(), containsInAnyOrder(shoppingCart.get(0), shoppingCart.get(2)));
    }

    @Test
    public void apply_should_discount_only_once_if_both_required_and_bundled_product_found_in_cart_multiple_times() {

        shoppingCart.add(new Item(VGA_ADAPTER));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));
        shoppingCart.add(new Item(VGA_ADAPTER));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(MAC_BOOK_PRO));

        Optional<PricingRuleResult> pricingRuleResultOptional = rule.apply(shoppingCart);

        assertThat(pricingRuleResultOptional.isPresent(), is(true));

        BigDecimal expectedDiscountedPrice = MAC_BOOK_PRO.getPrice();
        PricingRuleResult pricingRuleResult = pricingRuleResultOptional.get();
        assertThat(pricingRuleResult.getTotalPriceWithDiscount(), is(equalTo(expectedDiscountedPrice)));
        assertThat(pricingRuleResult.getDiscountedItems(), containsInAnyOrder(shoppingCart.get(0), shoppingCart.get(2)));
    }
}
