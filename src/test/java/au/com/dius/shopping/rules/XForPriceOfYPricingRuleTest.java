package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static au.com.dius.shopping.ProductFixture.APPLE_TV;
import static au.com.dius.shopping.ProductFixture.IPAD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class XForPriceOfYPricingRuleTest {

    private XForPriceOfYPricingRule rule;
    private List<Item> shoppingCart;

    @Before
    public void setUp() {
        shoppingCart = new ArrayList<>();
    }

    @Test(expected = RuntimeException.class)
    public void create_rule_should_throw_exception_when_inproperly_constructed() {
        rule = new XForPriceOfYPricingRule(APPLE_TV, 1, 10);
    }

    @Test
    public void apply_should_not_discount_if_shopping_cart_is_empty() {
        rule = new XForPriceOfYPricingRule(APPLE_TV, 3, 2);

        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_not_discount_if_number_of_applicable_products_is_less_than_threshold() {
        rule = new XForPriceOfYPricingRule(APPLE_TV, 3, 2);

        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(IPAD));

        assertThat(rule.apply(shoppingCart).isPresent(), is(false));
    }

    @Test
    public void apply_should_discount_if_number_of_applicable_products_is_equal_to_threshold() {
        rule = new XForPriceOfYPricingRule(APPLE_TV, 3, 2);

        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(APPLE_TV));

        Optional<PricingRuleResult> pricingRuleResultOptional = rule.apply(shoppingCart);

        assertThat(pricingRuleResultOptional.isPresent(), is(true));

        BigDecimal expectedDiscountedPrice = APPLE_TV.getPrice().multiply(BigDecimal.valueOf(2));
        PricingRuleResult pricingRuleResult = pricingRuleResultOptional.get();
        assertThat(pricingRuleResult.getTotalPriceWithDiscount(), is(equalTo(expectedDiscountedPrice)));
        assertThat(pricingRuleResult.getDiscountedItems(), containsInAnyOrder(shoppingCart.get(0), shoppingCart.get(1), shoppingCart.get(3)));
    }

    @Test
    public void apply_should_discount_only_once_if_number_of_applicable_products_are_in_multiples_of_threshold() {
        rule = new XForPriceOfYPricingRule(APPLE_TV, 3, 2);

        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(APPLE_TV));
        shoppingCart.add(new Item(IPAD));
        shoppingCart.add(new Item(APPLE_TV));

        Optional<PricingRuleResult> pricingRuleResultOptional = rule.apply(shoppingCart);

        assertThat(pricingRuleResultOptional.isPresent(), is(true));

        BigDecimal expectedDiscountedPrice = APPLE_TV.getPrice().multiply(BigDecimal.valueOf(2));
        PricingRuleResult pricingRuleResult = pricingRuleResultOptional.get();
        assertThat(pricingRuleResult.getTotalPriceWithDiscount(), is(equalTo(expectedDiscountedPrice)));
        assertThat(pricingRuleResult.getDiscountedItems(), containsInAnyOrder(shoppingCart.get(0), shoppingCart.get(1), shoppingCart.get(3)));
    }
}
