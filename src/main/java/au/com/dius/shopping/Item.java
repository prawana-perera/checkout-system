package au.com.dius.shopping;

import java.util.UUID;

public class Item {
    private UUID id;
    private Product product;

    public Item(Product product) {
        id = UUID.randomUUID();
        this.product = product;
    }

    public UUID getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }
}
