package au.com.dius.shopping;

import au.com.dius.shopping.rules.PricingRule;
import au.com.dius.shopping.rules.PricingRuleResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Checkout {

    private Map<String, Product> productCatalog;
    private List<PricingRule> pricingRules;
    private List<Item> shoppingCart;

    public Checkout(Map<String, Product> productCatalog, List<PricingRule> pricingRules) {
        this.productCatalog = productCatalog;
        this.pricingRules = pricingRules;
        this.shoppingCart = new ArrayList<>();
    }

    public void scan(String sku) {
        shoppingCart.add(new Item(productCatalog.get(sku)));
    }

    private void doApplyRule(List<PricingRuleResult> discountMatches, List<Item> remainingItemsToDiscount, PricingRule pricingRule) {

        if (!remainingItemsToDiscount.isEmpty()) {
            Optional<PricingRuleResult> pricingRuleResult = pricingRule.apply(remainingItemsToDiscount);

            pricingRuleResult.ifPresent(result -> {
                discountMatches.add(result);
                remainingItemsToDiscount.removeAll(result.getDiscountedItems());

                // recursively apply the rule until we can't get any more discounts.
                this.doApplyRule(discountMatches, remainingItemsToDiscount, pricingRule);
            });
        }
    }

    public BigDecimal total() {

        List<Item> remainingItemsToDiscount = new ArrayList<>(shoppingCart);
        List<PricingRuleResult> discountMatches = new ArrayList<>();

        pricingRules.forEach(pricingRule -> this.doApplyRule(discountMatches, remainingItemsToDiscount, pricingRule));

        BigDecimal discountedTotal = discountMatches.stream()
                .map(PricingRuleResult::getTotalPriceWithDiscount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return remainingItemsToDiscount.stream()
                .map(Item::getProduct)
                .map(Product::getPrice)
                .reduce(discountedTotal, BigDecimal::add);
    }
}

