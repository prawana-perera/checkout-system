package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;

import java.math.BigDecimal;
import java.util.List;

public class PricingRuleResult {

    private BigDecimal totalPriceWithDiscount;
    private List<Item> discountedItems;

    public PricingRuleResult(BigDecimal totalPriceWithDiscount, List<Item> discountedItems) {
        this.totalPriceWithDiscount = totalPriceWithDiscount;
        this.discountedItems = discountedItems;
    }

    public BigDecimal getTotalPriceWithDiscount() {
        return totalPriceWithDiscount;
    }

    public List<Item> getDiscountedItems() {
        return discountedItems;
    }
}
