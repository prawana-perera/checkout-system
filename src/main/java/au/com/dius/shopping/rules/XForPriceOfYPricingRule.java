package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;
import au.com.dius.shopping.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class XForPriceOfYPricingRule implements PricingRule {

    private Product product;
    private int x;
    private int y;

    public XForPriceOfYPricingRule(Product product, int x, int y) {

        if (x <= y) {
            throw new RuntimeException("x must be greater than y");
        }

        this.product = product;
        this.x = x;
        this.y = y;
    }

    @Override
    public Optional<PricingRuleResult> apply(List<Item> shoppingCart) {

        List<Item> discountableItems = shoppingCart
                .stream()
                .filter(item -> item.getProduct().getSku().equals(this.product.getSku()))
                .limit(x)
                .collect(Collectors.toList());

        if (discountableItems.size() != this.x) {
            return Optional.empty();
        }

        return Optional.of(new PricingRuleResult(getDiscountedPrice(), discountableItems));
    }

    private BigDecimal getDiscountedPrice() {
        return this.product.getPrice().multiply(BigDecimal.valueOf(this.y));
    }
}
