package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;

import java.util.List;
import java.util.Optional;

/**
 * A product pricing discount rule. A {@link PricingRule}s can be applied to a 'shopping cart', which list of {@link Item}s.
 * The rule is used to apply discounting rules.
 */
public interface PricingRule {

    Optional<PricingRuleResult> apply(List<Item> shoppingCart);
}
