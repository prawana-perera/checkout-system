package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;
import au.com.dius.shopping.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BundledProductPricingRule implements PricingRule {

    private Product requiredProduct;
    private Product bundledProduct;

    public BundledProductPricingRule(Product requiredProduct, Product bundledProduct) {
        this.requiredProduct = requiredProduct;
        this.bundledProduct = bundledProduct;
    }

    @Override
    public Optional<PricingRuleResult> apply(List<Item> shoppingCart) {

        Optional<Item> requiredItem = shoppingCart.stream()
                .filter(item -> item.getProduct().getSku().equals(requiredProduct.getSku()))
                .findFirst();

        Optional<Item> bundledItem = shoppingCart.stream()
                .filter(item -> item.getProduct().getSku().equals(bundledProduct.getSku()))
                .findFirst();

        if (!requiredItem.isPresent() || !bundledItem.isPresent()) {
            return Optional.empty();
        }

        List<Item> discountedItems = new ArrayList<>();
        discountedItems.add(requiredItem.get());
        discountedItems.add(bundledItem.get());

        return Optional.of(new PricingRuleResult(getDiscountedPrice(), discountedItems));
    }

    private BigDecimal getDiscountedPrice() {
        return requiredProduct.getPrice();
    }
}
