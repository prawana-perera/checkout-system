package au.com.dius.shopping.rules;

import au.com.dius.shopping.Item;
import au.com.dius.shopping.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BulkDiscountPricingRule implements PricingRule {

    private Product product;
    private int minNumberOfItems;
    private BigDecimal discountedPrice;

    public BulkDiscountPricingRule(Product product, int minNumberOfItems, BigDecimal discountedPrice) {
        this.product = product;
        this.minNumberOfItems = minNumberOfItems;
        this.discountedPrice = discountedPrice;
    }

    @Override
    public Optional<PricingRuleResult> apply(List<Item> shoppingCart) {

        List<Item> discountableItems = shoppingCart.stream()
                .filter(item -> item.getProduct().getSku().equals(product.getSku()))
                .collect(Collectors.toList());

        if (discountableItems.size() >= minNumberOfItems) {
            return Optional.of(new PricingRuleResult(getPrice(discountableItems), discountableItems));
        }

        return Optional.empty();
    }

    private BigDecimal getPrice(List<Item> applicableItems) {
        return this.discountedPrice.multiply(BigDecimal.valueOf(applicableItems.size()));
    }
}
