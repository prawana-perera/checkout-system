# Description #
This repository contains an example implementation of an Shopping Cart application which has the ability to use pricing rules in order to apply discounts.


### Application Design ###

The application design was kept simple. 

* The pricing rules were implemented using the Strategy pattern. See the `PricingRule` class and its implementations.
* The `Checkout` class is the main driver of the application, which was configured using both the pricing rules and the product catalog.
* The `Checkout` class delegated to the `PricingRule` strategy to calculate the applicable discounts.
* The operation of the system is demonstrated using tests, including `CheckoutIntegrationTest`, which contains the scenarios specified in the requirements as well as additional scenarios.

The use of vast frameworks or libraries were avoided as it was not needed for this exercise. Only a small handful of libraries were used, mainly to drive tests (JUnit, Harmcrest). Gradle was used the build tool.

### How do I get set up? ###

In order to setup and run you will need:

* Java 8
* A internet connection
* If you are behind a corporate proxy, you may need to add proxy configuration (see Gradle documentation)

To setup and run:

* Clone this repository
* Open a terminal and change directory to where you have cloned it
* Run `./gradlew build` (Linux/Mac) or `gradlew build` (Windows)

The above will run all tests and demonstrate the operation of the application. Navigate to the project's `build/reports/tests/test` directory and open the `index.html` file in a browser to see the result of test executions.